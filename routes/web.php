<?php

use App\Models\Product;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $events = Product::groupByRaw('DATE(created_at)')
        ->selectRaw('DATE(created_at) as start')
        ->selectRaw('count(DATE(created_at)) as title')
        ->get();
    return view('welcome', [
        'events' => $events
    ]);
});

Route::get('/products', [\App\Http\Controllers\ProductController::class, 'index']);

Route::get('/products/create', [\App\Http\Controllers\ProductController::class, 'create']);
Route::get('/products/edit/{product}', [\App\Http\Controllers\ProductController::class, 'edit']);

Route::post('/products', [\App\Http\Controllers\ProductController::class, 'store']);
