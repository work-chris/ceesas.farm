<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  <title>Productos</title>
</head>
<body>
<div class="container">
  <h1>Crear producto</h1>
  <div class="row">
    <div class="col">
      <form action="{{url('products')}}" method="{{isset($product) ? 'put' : 'post' }}">
        @csrf
        <x-danger-alert></x-danger-alert>
        <div class="mb-3">
          <label for="reference" class="form-label">Referencia</label>
          <input class="form-control" type="text" name="reference" value="{{optional($product)->reference}}" required {{isset($product) ? 'disabled': null}}>
        </div>
        <div class="mb-3">
          <label for="name" class="form-label">Nombre</label>
          <input class="form-control" type="text" name="name" value="{{optional($product)->name}}" required {{isset($product) ? 'disabled': null}}>
        </div>
        <div class="mb-3">
          <label for="laboratory_name" class="form-label">Laboratorio</label>
          <input class="form-control" type="text" name="laboratory_name" value="{{optional($product)->laboratory_name}}" required {{isset($product) ? 'disabled': null}}>
        </div>
        <div class="mb-3">
          <label for="die_date" class="form-label">Fecha de vencimiento</label>
          <input class="form-control" type="date" name="die_date" min="{{now()->format('Y-m-d')}}" value="{{optional($product)->die_date}}" required>
        </div>
        <div class="mb-3">
          <label for="income_quantity" class="form-label">Cantidad ingresada</label>
          <input class="form-control" type="number" name="income_quantity" value="{{optional($product)->income_quantity}}" required>
        </div>
        <div class="d-grid gap-2 d-md-flex justify-content-md-end">
          <button type="submit" class="btn btn-success">Crear</button>
        </div>
      </form>
    </div>
  </div>
</div>
</body>
</html>