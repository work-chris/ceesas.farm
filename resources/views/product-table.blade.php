<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css" integrity="sha512-Fo3rlrZj/k7ujTnHg4CGR2D7kSs0v4LLanw2qksYuRlEzO+tcaEPQogQ0KaoGN26/zrn20ImR1DfuLWnOo7aBA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <title>Productos</title>
</head>
<body>
<div class="container-fluid">
  <h1>Inventario de productos</h1>
  <div class="row">
    <div class="col-3">
      <form>
        <div class="mb-3">
          <label for="reference" class="form-label">Referencia</label>
          <input class="form-control" type="text" name="reference" value="{{$filter['reference']}}">
        </div>
        <div class="mb-3">
          <label for="name" class="form-label">Nombre</label>
          <input class="form-control" type="text" name="name" value="{{$filter['name']}}">
        </div>
        <div class="mb-3">
          <label for="laboratory_name" class="form-label">Laboratorio</label>
          <select name="laboratory_name" class="form-control">
            <option></option>
            @foreach($laboratories as $laboratory)
            <option value="{{$laboratory}}" {{$filter['laboratory'] == $laboratory ? 'selected' : null}}>{{$laboratory}}</option>
            @endforeach
          </select>
        </div>
        <div class="d-grid gap-2 d-md-flex justify-content-md-end">
          <button type="reset" class="btn btn-info">Limpiar</button>
          <button type="submit" class="btn btn-success">Filtrar</button>
        </div>
      </form>
    </div>
    <div class="col">
      <div class="container">
        <div class="row">
          <div class="d-grid gap-2 d-md-flex justify-content-md-end">
            <a type="button" class="btn btn-outline-success" href="{{url('products/create')}}">
              <i class="fa-solid fa-square-plus"></i> Nuevo
            </a>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <table class="table">
              <thead>
              <tr>
                <th>#</th>
                <th>Reference</th>
                <th>Nombre</th>
                <th>Laboratorio</th>
                <th>Fec.Ven.</th>
                <th>Cant. Ing.</th>
              </tr>
              </thead>
              <tbody>
              @foreach($products as $product)
                <tr>
                  <td>{{$product->id}}</td>
                  <td>{{$product->reference}}</td>
                  <td>{{$product->name}}</td>
                  <td>{{$product->laboratory_name}}</td>
                  <td>{{$product->die_date}}</td>
                  <td>{{$product->income_quantity}}</td>
                  <td>
                    <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                      <a type="button" class="btn btn-sm btn-outline-dark" href="{{url('products/edit/'. $product->id)}}">
                        <i class="fa-solid fa-pen-to-square"></i>
                      </a>
                    </div>
                  </td>
                </tr>
              @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
</html>