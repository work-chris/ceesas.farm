<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'reference'=> $this->faker->bothify('??###?#?'),
            'name'=> $this->faker->colorName,
            'laboratory_name'=> $this->faker->randomElement(['Laboratorio 1', 'Laboratorio 2']),
            'die_date'=> $this->faker->creditCardExpirationDate,
            'income_quantity'=> $this->faker->randomNumber(2)
        ];
    }
}
