<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $products = \App\Models\Product::where('die_date', '>', now());
    
        if ($reference = $request->get('reference')) {
            $products->where('reference', '=', $reference);
        }
        if ($laboratory = $request->get('laboratory_name')) {
            $products->where('laboratory_name', '=', $laboratory);
        }
        if ($name = $request->get('name')) {
            $products->where('name', 'ilike', '%'.$name. '%');
        }
        
        return view('product-table', [
            'laboratories' => Product::groupBy('laboratory_name')->pluck('laboratory_name'),
            'products' => $products->get(),
            'filter' => [
                'reference' => $reference,
                'name' => $name,
                'laboratory' => $laboratory,
            ]
        ]); // resources/vies/products.blade.php
    }
    
    public function create()
    {
        return view('product-form', [
            'product' => null
        ]);
    }
    
    public function edit(Product $product)
    {
        return view('product-form',[
            'product' => $product
        ]);
    }
    
    public function store(Request $request)
    {
        $reference = $request->post('reference');
        
        $count = Product::where('reference', $reference)
            ->where('created_at', '>=', now()->subHours(24))
            ->count();
        
        if ($count) {
            return redirect()
                ->back()
                ->withErrors('Esta referencia fue registrada en menos de 24 horas.');
        }
        
        Product::create($request->all());
        
        return redirect('products');
    }
}
